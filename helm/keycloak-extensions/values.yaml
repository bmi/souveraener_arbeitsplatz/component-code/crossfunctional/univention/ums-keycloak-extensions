# SPDX-License-Identifier: AGPL-3.0-only
# SPDX-FileCopyrightText: 2024 Univention GmbH

---
# -- Global Keycloak Extensions configuration values
global:
  # -- Indicates wether this chart is part of a Nubus deployment.
  nubusDeployment: false
  keycloak:
    realm: ""
  postgresql:
    connection:
      host: ""
      port: ""

# -- PostgreSQL settings.
postgresql:
  # -- Connection parameters.
  connection:
    # -- PostgreSQL host.
    host: ""
    # -- PostgreSQL port.
    port: ""
    # -- PostgreSQL SSL flag
    ssl: "false"
    # -- Path to CA
    pathCA: "/etc/ssl/certs/rootca.pem"
    # -- CustomCA certificate
    customca: ""
  auth:
    # -- PostgreSQL user.
    username: ""
    # -- PostgreSQL database.
    database: ""
    # -- PostgreSQL user password.
    # password: ""
    # -- PostgreSQL password secret reference.
    existingSecret:
      name: ""
      keyMapping:
        password: null

# -- Keycloak settings.
keycloak:
  # -- Connection parameters.
  connection:
    # -- Keycloak host.
    host: ""
  auth:
    # -- Keycloak user.
    username: ""
    # -- Keycloak password.
    # password: ""
    # -- Keycloak realm.
    realm: ""
    # -- Keycloak master realm.
    masterRealm:
      "master"
      # -- Keycloak password secret reference.
    existingSecret:
      name: ""
      keyMapping:
        adminPassword: null

# -- SMTP settings.
smtp:
  # -- Connection parameters.
  connection:
    # -- Email SMTP hostname
    host: ""
    # -- Email SMTP port
    port: "587"
    # -- Require SSL/TLS encryption for connection.
    ssl: false
    # -- Use StartTLS for traffic encryption:
    starttls: true
  auth:
    # -- Enable SMTP authentication
    enabled: true
    # -- Username for SMTP authentication
    username: ""
    # -- Password for SMTP authentication
    # password: ""
    # -- SMTP password secret reference.
    existingSecret:
      name: ""
      keyMapping:
        password: null

handler:
  enabled: true
  ## Global values
  affinity: {}
  environment: {}
  nodeSelector: {}
  podAnnotations: {}
  podSecurityContext: {}
  replicaCount: 1
  securityContext:
    allowPrivilegeEscalation: false
    capabilities:
      drop:
        - ALL
    privileged: false
    readOnlyRootFilesystem: true
    runAsGroup: 1000
    runAsNonRoot: true
    runAsUser: 1000
    seccompProfile:
      type: RuntimeDefault
  tolerations: []
  # -- Additional custom annotations to add to deployments.
  additionalAnnotations: {}
  serviceAccount:
    # Specifies whether a service account should be created
    create: true
    # Annotations to add to the service account
    annotations: {}
    # The name of the service account to use.
    # If not set and create is true, a name is generated using the fullname template
    name: ""
    ## @param serviceAccount.automountServiceAccountToken Allows auto mount of ServiceAccountToken on the serviceAccount created
    ## Can be set to false if pods using this serviceAccount do not need to use K8s API
    ##
    automountServiceAccountToken: false
    # -- Additional custom labels for the ServiceAccount.
    labels: {}
  ## Configure extra options for liveness, readiness and startup probes
  ## @param handler.livenessProbe.enabled Enable livenessProbe on PostgreSQL Primary containers
  ## @param handler.livenessProbe.initialDelaySeconds Initial delay seconds for livenessProbe
  ## @param handler.livenessProbe.periodSeconds Period seconds for livenessProbe
  ## @param handler.livenessProbe.timeoutSeconds Timeout seconds for livenessProbe
  ## @param handler.livenessProbe.failureThreshold Failure threshold for livenessProbe
  ## @param handler.livenessProbe.successThreshold Success threshold for livenessProbe
  ## @param handler.livenessProbe.command Command for the livenessProbe
  ##
  livenessProbe:
    enabled: false
    initialDelaySeconds: 30
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
    command: |
      exit 0
  ## @param handler.readinessProbe.enabled Enable readinessProbe on PostgreSQL Primary containers
  ## @param handler.readinessProbe.initialDelaySeconds Initial delay seconds for readinessProbe
  ## @param handler.readinessProbe.periodSeconds Period seconds for readinessProbe
  ## @param handler.readinessProbe.timeoutSeconds Timeout seconds for readinessProbe
  ## @param handler.readinessProbe.failureThreshold Failure threshold for readinessProbe
  ## @param handler.readinessProbe.successThreshold Success threshold for readinessProbe
  ## @param handler.readinessProbe.command Command for the readinessProbe
  ##
  readinessProbe:
    enabled: false
    initialDelaySeconds: 5
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
    command: |
      exit 0
  ## @param handler.startupProbe.enabled Enable startupProbe on PostgreSQL Primary containers
  ## @param handler.startupProbe.initialDelaySeconds Initial delay seconds for startupProbe
  ## @param handler.startupProbe.periodSeconds Period seconds for startupProbe
  ## @param handler.startupProbe.timeoutSeconds Timeout seconds for startupProbe
  ## @param handler.startupProbe.failureThreshold Failure threshold for startupProbe
  ## @param handler.startupProbe.successThreshold Success threshold for startupProbe
  ## @param handler.startupProbe.command Command for the startupProbe
  ##
  startupProbe:
    enabled: false
    initialDelaySeconds: 30
    periodSeconds: 10
    timeoutSeconds: 1
    failureThreshold: 15
    successThreshold: 1
    command: |
      exit 0
  ## @param handler.customLivenessProbe Custom livenessProbe that overrides the default one
  ##
  customLivenessProbe: {}
  ## @param handler.customReadinessProbe Custom readinessProbe that overrides the default one
  ##
  customReadinessProbe: {}
  ## @param handler.customStartupProbe Custom startupProbe that overrides the default one
  ##
  customStartupProbe: {}
  ## @param handler.lifecycleHooks for the handler container to automate configuration before or after startup
  ##
  lifecycleHooks: {}
  # -- Application configuration of the Handler
  # -- In seconds, time the given to the pod needs to terminate gracefully.
  # Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
  terminationGracePeriodSeconds: ""

  # -- Application configuration of the handler
  appConfig:
    # -- Application LOG level: `DEBUG`, `INFO`, `WARN` or `ERROR`
    logLevel: "DEBUG"
    # -- Number of failed login attempts within the minutes of `eventsRetentionPeriod` to trigger an IP block. Should be greater than `failedAttemptsForDeviceBlock` if it is enabled
    failedAttemptsForIpBlock: 7
    # -- Number of failed login attempts within the minutes of `eventsRetentionPeriod` to trigger a device block. Should be greater than `failedAttemptsForCaptchaTrigger` if it is enabled
    failedAttemptsForDeviceBlock: 5
    # -- Number of failed login attempts within the minutes of `eventsRetentionPeriod` to enforce reCaptcha prompt
    failedAttemptsForCaptchaTrigger: 3
    # -- Minutes to buffer Keycloak events locally, allowing to persist more than the configured in Keycloak
    eventsRetentionPeriod: 1
    # -- Minutes to automatically expire actions such as IP and device blocks and reCaptcha prompt
    autoExpireRuleInMins: 1
    # -- Whether to enable device blocking
    deviceProtectionEnable: "True"
    # -- Whether to enable IP blocking
    ipProtectionEnable: "True"
    # -- Whether to enable reCaptcha prompting protection
    captchaProtectionEnable: "False"
    # -- Whether to enable email notification to users on New Device Login
    newDeviceLoginNotificationEnable: "True"
    # -- Email to send emails from
    mailFrom: "univention@example.org"
    # -- Subject for email notification to users on New Device Login
    newDeviceLoginSubject: "New device login"

  # -- Kubernetes ingress
  ingress:
    # -- Set this to `true` in order to enable the installation on Ingress related objects.
    enabled: false

  ## Docker image
  image:
    # -- Container registry address.
    registry: "artifacts.software-univention.de"
    repository: "nubus-dev/images/keycloak-handler"
    tag: "latest"
    imagePullPolicy: "IfNotPresent"

  # -- Credentials to fetch images from private registry.
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  imagePullSecrets: []

  ## Deployment resources
  resources:
    requests:
      memory: "512Mi"
      cpu: "250m"
    limits:
      memory: "4Gi"
      cpu: "4"

  ## Kubernetes service
  service:
    enabled: false
    # -- Additional custom annotations to add to service.
    additionalAnnotations: {}

proxy:
  enabled: true
  affinity: {}
  environment: {}
  nodeSelector: {}
  podAnnotations: {}
  podSecurityContext: {}
  replicaCount: 1
  securityContext:
    allowPrivilegeEscalation: false
    capabilities:
      drop:
        - ALL
    privileged: false
    readOnlyRootFilesystem: true
    runAsGroup: 1000
    runAsNonRoot: true
    runAsUser: 1000
    seccompProfile:
      type: RuntimeDefault
  tolerations: []
  # -- Additional custom annotations to add to deployments.
  additionalAnnotations: {}
  serviceAccount:
    # Specifies whether a service account should be created
    create: true
    # Annotations to add to the service account
    annotations: {}
    # The name of the service account to use.
    # If not set and create is true, a name is generated using the fullname template
    name: ""
    ## @param serviceAccount.automountServiceAccountToken Allows auto mount of ServiceAccountToken on the serviceAccount created
    ## Can be set to false if pods using this serviceAccount do not need to use K8s API
    ##
    automountServiceAccountToken: false
    # -- Additional custom labels for the ServiceAccount.
    labels: {}
  ## Configure extra options for liveness, readiness and startup probes
  ## @param proxy.livenessProbe.enabled Enable livenessProbe on PostgreSQL Primary containers
  ## @param proxy.livenessProbe.initialDelaySeconds Initial delay seconds for livenessProbe
  ## @param proxy.livenessProbe.periodSeconds Period seconds for livenessProbe
  ## @param proxy.livenessProbe.timeoutSeconds Timeout seconds for livenessProbe
  ## @param proxy.livenessProbe.failureThreshold Failure threshold for livenessProbe
  ## @param proxy.livenessProbe.successThreshold Success threshold for livenessProbe
  ##
  livenessProbe:
    enabled: false
    initialDelaySeconds: 30
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## @param proxy.readinessProbe.enabled Enable readinessProbe on PostgreSQL Primary containers
  ## @param proxy.readinessProbe.initialDelaySeconds Initial delay seconds for readinessProbe
  ## @param proxy.readinessProbe.periodSeconds Period seconds for readinessProbe
  ## @param proxy.readinessProbe.timeoutSeconds Timeout seconds for readinessProbe
  ## @param proxy.readinessProbe.failureThreshold Failure threshold for readinessProbe
  ## @param proxy.readinessProbe.successThreshold Success threshold for readinessProbe
  ##
  readinessProbe:
    enabled: false
    initialDelaySeconds: 5
    periodSeconds: 10
    timeoutSeconds: 5
    failureThreshold: 6
    successThreshold: 1
  ## @param proxy.startupProbe.enabled Enable startupProbe on PostgreSQL Primary containers
  ## @param proxy.startupProbe.initialDelaySeconds Initial delay seconds for startupProbe
  ## @param proxy.startupProbe.periodSeconds Period seconds for startupProbe
  ## @param proxy.startupProbe.timeoutSeconds Timeout seconds for startupProbe
  ## @param proxy.startupProbe.failureThreshold Failure threshold for startupProbe
  ## @param proxy.startupProbe.successThreshold Success threshold for startupProbe
  ##
  startupProbe:
    enabled: false
    initialDelaySeconds: 30
    periodSeconds: 10
    timeoutSeconds: 1
    failureThreshold: 15
    successThreshold: 1
  ## @param proxy.customLivenessProbe Custom livenessProbe that overrides the default one
  ##
  customLivenessProbe: {}
  ## @param proxy.customReadinessProbe Custom readinessProbe that overrides the default one
  ##
  customReadinessProbe: {}
  ## @param proxy.customStartupProbe Custom startupProbe that overrides the default one
  ##
  customStartupProbe: {}
  ## @param handler.lifecycleHooks for the proxy container to automate configuration before or after startup
  ##
  lifecycleHooks: {}
  # -- In seconds, time the given to the pod needs to terminate gracefully.
  # Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
  terminationGracePeriodSeconds: ""

  # -- Application configuration of the proxy
  appConfig:
    # -- Proxy log level: `debug`, `info`, `warn` or `error`
    logLevel: "debug"
    # -- The Google reCaptcha v2 site key generated from [their admin site](https://www.google.com/recaptcha/admin/)
    captcha:
      captchaSiteKey: "some_site_key"
      captchaSecretKey: "some_secret_key"
      existingSecret:
        name: ""
        keyMapping:
          site_key: null
          secret_key: null

  # -- Kubernetes ingress
  ingress:
    # -- Set this to `true` in order to enable the installation on Ingress related objects.
    enabled: true
    ingressClassName: ""
    # Request certificates via cert-manager.io annotation
    certManager:
      # -- Enable cert-manager.io annotaion.
      enabled: true

      # Issuer reference.
      issuerRef:
        # -- Name of cert-manager.io Issuer resource.
        name: ""
        # -- Type of Issuer, f.e. "Issuer" or "ClusterIssuer".
        kind: "ClusterIssuer"
    annotations:
      nginx.org/proxy-buffer-size: "8k"
      nginx.ingress.kubernetes.io/proxy-buffer-size: "8k"
    ## define hostname
    # host: "sso.example.com"
    paths:
      - pathType: "Prefix"
        path: "/admin"
      - pathType: "Prefix"
        path: "/realms"
      - pathType: "Prefix"
        path: "/resources"
      - pathType: "Prefix"
        path: "/fingerprintjs"
    tls:
      enabled: true
      secretName: ""

  ## Docker image
  image:
    # -- Container registry address.
    registry: "artifacts.software-univention.de"
    repository: "nubus-dev/images/keycloak-proxy"
    tag: "latest"
    imagePullPolicy: "IfNotPresent"

  # -- Credentials to fetch images from private registry.
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  imagePullSecrets: []

  ## Deployment resources
  resources:
    requests:
      memory: "512Mi"
      cpu: "250m"
    limits:
      memory: "4Gi"
      cpu: "4"

  ## Kubernetes service
  service:
    enabled: true
    type: "ClusterIP"
    # -- Additional custom annotations to add to service.
    additionalAnnotations: {}
    ports:
      http:
        containerPort: 8181
        port: 8181
        protocol: "TCP"

    sessionAffinity:
      enabled: false
      timeoutSeconds: 10800
